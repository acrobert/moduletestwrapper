from Keithley2410 import Keithley2410
import matplotlib.pyplot as plt

if __name__ == "__main__":
	sourcemeter = Keithley2410()
	time_array = [0.5, 1, 2, 5, 10]
	Vmin = 0
	Vmax = 800
	steps = 20
	Ilimit = 105e-6

	plt.ion()

	for t in time_array:
		if not sourcemeter.get_enable_tripped():
			print("Close box lid")
			exit()
		sweep_data = sourcemeter.voltage_sweep(Vmin, Vmax, steps, Ilimit, t)
		x_axis = []
		y_axis = []
		for element in sweep_data:
			x_axis.append(float(element['voltage']))
			y_axis.append(float(element['current'])*1e6)
		plt.plot(x_axis, y_axis, label=f"{t}s")

	plt.xlabel("Voltage [V]")
	plt.ylabel("Current [uA]")
	plt.yscale("log")
	plt.legend()
	plt.show()
	sourcemeter.shutdown()
