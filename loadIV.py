import PySimpleGUI as sg
from Keithley2400 import Keithley2400
import numpy as np
import time
import matplotlib.pyplot as plt
import pickle
import glob
import datetime
from time import sleep
from argparse import ArgumentParser

lgfont = ('Arial', 30)

# Parse arguments or prompt user for input
parser = ArgumentParser(prog = 'IV Set', description = 'Set of IV curves for live modules')

parser.add_argument('-n', '--name', default=None, help='Name of module', type=str)
parser.add_argument('-t', '--tag', default=None, help='Tag for this specific IV curve set', type=str)
parser.add_argument('-d', '--date', default=None, help='Date curves were taken', type=str)

args = parser.parse_args()

if args.name != None:
    modulename = args.name
else:
    modulename = input('>> Enter module name (e.g. CMU-ESR2): ')

if args.tag != None:
    tag = args.tag
else:
    tag = input('>> Enter label for IV curve set: ')

if args.date != None:
    date = args.date
else:
    date = input('>> Enter date curves were taken (yyyy-mm-dd): ')

# Glob for stored curves based on arguments
print(f'moduleIV/{modulename}/{modulename}_IVset_{date}_{tag}_*.pkl')
pickles_to_load = glob.glob(f'moduleIV/{modulename}/{modulename}_IVset_{date}_{tag}_*.pkl')
print(f'>> Loading pickles: {pickles_to_load}')
datadict = pickle.load(open(pickles_to_load[0], 'rb'))
# Reorder to match order taken
pickles_to_load = [pickles_to_load[1], pickles_to_load[0]]

# Plot 
for pick in pickles_to_load:
    datadict = pickle.load(open(pick, 'rb'))
    data = datadict['data']
    plt.plot(data[:,1], data[:,2]*1000000., 'o-', label='RH '+datadict['RH'])

plt.yscale('log')
plt.title(f'{modulename} module IV Curve Set {datadict["date"]}')
plt.xlabel('Bias Voltage [V]')
plt.ylabel(r'Leakage Current [$\mu$A]')
plt.ylim(0.01, 100)
plt.xlim(0, 800)
plt.legend()
plt.savefig(f'moduleIV/{modulename}/{modulename}_loadIVset_{datadict["date"]}_{tag}.png')
plt.show()

layout = [[sg.Text(f'IV Plot for {modulename}', font=lgfont)],                                                                                                    
          [sg.Image(f'moduleIV/{modulename}/{modulename}_loadIVset_{datadict["date"]}_{tag}.png')]]                                   
window = sg.Window(f"Module Test: IV Plot", layout, margins=(200,100))                                                                                                          
                                                                                                                                                                                
event, values = window.read(timeout=100)                                                                                                                                        

sleep(5)
for i in range(100):
    sleep(1)
    print(i)

    
