#include <Wire.h>
#include "SparkFun_Qwiic_Relay.h"
#include "SparkFunBME280.h"
#define RELAY_ADDR 0x18 // Alternate address 0x19

BME280 humidity0;
Qwiic_Relay relay(RELAY_ADDR); 
String command;

void setup() {
	Wire.begin(); 
	Serial.begin(115200);
	humidity0.settings.I2CAddress = 0x77;

	//Check sensors
	if(!relay.begin() or !humidity0.beginI2C()) {
		Serial.println("Error");
		return -1;
	}
	
	relay.turnRelayOff();
}

void loop() {
	if (Serial.available() > 0) {
		command = Serial.readStringUntil('\n');
		if (command == "air on") {
			relay.turnRelayOn();
		}
		else if (command == "air off") {
			relay.turnRelayOff();
		}
		else {
			Serial.println("Error");
		}
	}
  int humidity = humidity0.readFloatHumidity();
  int temp_c = humidity0.readTempC();
  Serial.print(humidity);
  Serial.print(",");
  Serial.print(temp_c);
	Serial.println();
	delay(1000);
}