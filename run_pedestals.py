import paramiko
import time
import os
import subprocess
import sys
import glob
from TrenzTestStand import TrenzTestStand
from CentosPC import CentosPC
from argparse import ArgumentParser
from Keithley2400 import Keithley2400
from time import sleep

import yaml
configuration = {}
with open('configuration.yaml', 'r') as file:
    configuration = yaml.safe_load(file)


# Parse the arguments
# can add them in command line; if not, terminal prompted for input for necessary fields
parser = ArgumentParser(prog = 'Script to run pedestals', description = 'Run pedestals using Trenz test stand and lab PC.')

parser.add_argument('-m', '--modulename', default=None, help='Name of module', type=str)
#parser.add_argument('-k', '--keyloc', default='/home/hgcal/.ssh/id_rsa', help='Location of private ssh key for connecting to Trenz', type=str)
parser.add_argument('-o', '--hostname', default=configuration['TrenzHostname'][0], help='IP address of Trenz test stand', type=str)

args = parser.parse_args()

#keyloc = args.keyloc
hostname = args.hostname
modulename = args.modulename

if modulename is None:
    print('Enter module name!')
    exit()

print('>> Initialize test stand')
ts = TrenzTestStand(hostname)

print('>> Configure test stand and hexaboard')
fwloaded = ts.loadfw()
if not fwloaded:
    print('-- Error in firmware load. Exiting')
    exit()
services = ts.startservers()
if not services:
    print('-- Error in test stand services. Exiting')
    exit()

print('>> Connecting to power supply')
ps = Keithley2400()
print('>> Run pedestals')

if 'ML' in modulename:
    ps.setVoltage(300.)
    ps.outputOn()
pc = CentosPC(hostname, modulename, live=('ML' in modulename))

pc.pedestal_run(BV=300)
pc.pedestal_scan()
pc.vrefnoinv_scan()
pc.vrefinv_scan()

BVs = [0, 100, 200, 300, 400, 300, 300]
for i in range(len(BVs)):

    if 'ML' in modulename:
        ps.setVoltage(BVs[i])
        sleep(1)
        pc.pedestal_run(BV=BVs[i])
        pc.make_hexmaps(BV=BVs[i])
    else:
        pc.pedestal_run()
        pc.make_hexmaps()

print(ts.status())
print(ts.statusservers())
print(pc.status_daq())

if 'ML' in modulename:
    ps.outputOff()
input('   Unplug DCDC power')
out, err = ts.shutdown()
input('   Unplug hexacontroller power')
print('>> Can now disconnect all parts')
