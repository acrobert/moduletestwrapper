from Keithley2400 import Keithley2400
import numpy as np
import time
import matplotlib.pyplot as plt
import pickle
from argparse import ArgumentParser
import datetime
from dbtools import save_and_upload
current_date = datetime.datetime.now()
date = current_date.isoformat().split('T')[0]
nowtime = current_date.isoformat().split('T')[1].split('.')[0]

# Parse the arguments
# can add them in command line; if not, terminal prompted for input for necessary fields
parser = ArgumentParser(prog = 'IV Set', description = 'Set of IV curves for live modules')

parser.add_argument('-n', '--name', default=None, help='Name of module', type=str)
parser.add_argument('-t', '--tag', default=None, help='Tag for this specific IV curve set', type=str)
parser.add_argument('-h', '--humidity', default=None, help='Relative humidity at the beginning', type=str)
parser.add_argument('-i', '--inspector', default=None, help='Person performing the test', type=str)
parser.add_argument('-s', '--step', default=20, help='Voltage step', type=int)
parser.add_argument('-v', '--maxv', default=800, help='Max voltage', type=int)
parser.add_argument('-d', '--drytime', default=20, help='Time to wait after dry air activated in minutes', type=int)

args = parser.parse_args()

if args.name != None:
    modulename = args.name
else:
    modulename = input('>> Enter module name (e.g. CMU-ESR2): ')

if args.tag != None:
    tag = args.tag
else:
    tag = input('>> Enter label for this IV curve: ')

if args.humidity != None:
    RH0 = args.humidity
else:
    RH0 = input('>> Enter initial relative humidity value: ')

if args.inspector != None:
    inspector = args.inspector
else:
    inspector = input('>> Enter your name: ')
    
# IV curve settings taken from default arguments
step = args.step
ln = int(args.maxv//step)+1
final_dry_time = 60*args.drytime

# Initialize power supply wrapper
ps = Keithley2400()
print('>> Keithley2400 connected')

# Check safety switch
HVswitch_tripped = ps.switch_state()
if not HVswitch_tripped:
    print('>> HV switch not tripped - exiting. Please close box and try again.')
    exit()

# Take wet curve
print('>> Taking ambient IV')
wetcurve = ps.takeIV(800, step, RH0)
save_and_upload(wetcurve, modulename, tag, RH0, inspector)

# Check safety switch
HVswitch_tripped = ps.switch_state()
if not HVswitch_tripped:
    print('>> HV switch not tripped - exiting. Please close box and try again.')
    exit()

# Start dry air and wait
input('Open dry air valve and press enter')
drytime = time.time()
dry_date = datetime.datetime.now()
finalIV_date = dry_date + datetime.timedelta(seconds=final_dry_time)
finalIV_time = finalIV_date.isoformat().split('T')[1].split('.')[0]

time_to_wait = final_dry_time - (time.time() - drytime)
print(f'>> Waiting until {finalIV_time} to perform final IV')
time.sleep(time_to_wait)

# Check safety switch
HVswitch_tripped = ps.switch_state()
if not HVswitch_tripped:
    print('>> HV switch not tripped - exiting. Please close box and try again.')
    exit()

# Take dry curve
print('>> Taking dry IV')
drycurve = ps.takeIV(800, step, '0')
save_and_upload(drycurve, modulename, tag, '0', inspector)

# Plot
for datadict in ps.IVdata:
    data = datadict['data']
    #with open(f'moduleIV/{modulename}_IVset_{datadict["date"]}_{tag}_{datadict["RH"]}.pkl', 'wb') as datafile:
    #    pickle.dump(datadict, datafile)
    #print(datadict)
    plt.plot(data[:,1], data[:,2]*1000000., 'o-', label='RH '+datadict['RH'])

plt.yscale('log')
plt.title(f'{modulename} module IV Curve Set {date}')
plt.xlabel('Bias Voltage [V]')
plt.ylabel(r'Leakage Current [$\mu$A]')
plt.ylim(0.01, 100)
plt.xlim(0, 800)
plt.legend()
plt.savefig(f'moduleIV/{modulename}_IVset_{datadict["date"]}_{tag}.pdf')
plt.show()




    
