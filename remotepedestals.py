import paramiko
import time
import os
import subprocess
import sys
import glob
sys.path.insert(1, '/home/hgcal/hexmap')
from plot_summary import make_hexmap_plots_from_file

keyloc = '/home/hgcal/.ssh/id_rsa'
IP = 'cmshgcaltb4.lan.local.cmu.edu'
modulename = 'TWL06'

print(f'>> Connecting to {IP}...')

# wait until can ping test stand
connected = False
while not connected:
    response = os.system("ping -c 1 " + IP + " >/dev/null 2>&1")
    if response == 0:
        connected = True
        

# create ssh client
ssh = paramiko.SSHClient()
k = paramiko.RSAKey.from_private_key_file(keyloc)
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname='cmshgcaltb4.lan.local.cmu.edu', username='root', pkey=k)

print('>> Connected.')

#sh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('source runV3software.sh')
#ime.sleep(5)
#sh_stdin.close()

input('   Connect DCDC power cord and press enter.')

ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('fw-loader load hexaboard-hd-tester-v1p1-trophy-v3 && listdevice')

#print(ssh_stdout.readlines())
#print(ssh_stderr.readlines())

firmware_loaded = False
channels_found = False
for line in ssh_stdout.readlines():

    # check FW load
    if 'Loaded the device tree overlay successfully using the zynqMP FPGA manager' in line:
        print('>> Loaded firmware')
        firmware_loaded = True

    # check channels in listdevice
    if '00: -- -- -- -- -- -- -- -- 08 09 0a 0b 0c 0d 0e 0f' in line:
        print('>> Discovered ROC channels')
        channels_found = True

    if firmware_loaded and channels_found:
        break

#print(firmware_loaded, channels_found)
#exit()

ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('systemctl restart daq-server.service && systemctl restart i2c-server.service && sleep 5 && systemctl status daq-server.service && systemctl status i2c-server.service')
#ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('systemctl status i2c-server.service && systemctl status daq-server.service')

board_discovered = False
daq_initiated = False
error_check = False

if len(ssh_stderr.readlines()) == 0:
    error_check = True

for line in ssh_stdout.readlines():

    if '[I2C] Board identification: V3 LD Full HB' in line:
        print('>> Identified LD HexaBoard')
        board_discovered = True

    if 'Started daq-client start/stop service script.' in line:
        print('>> DAQ server initiated')
        daq_initiated = True

    if 'Error' in line or 'error' in line:
        error_check = False
        break

if not (board_discovered and daq_initiated and error_check):
    exit()

print('>> Services ready to receive and send data.')

os.system('systemctl restart daq-client.service')

print('>> DAQ client started. Running pedestals...')
os.system(f'source /opt/hexactrl/ROCv3/ctrl/etc/env.sh && python3 /opt/hexactrl/ROCv3/ctrl/pedestal_run.py -i {IP} -f /opt/hexactrl/ROCv3/ctrl/etc/configs/initLD-trophyV3.yaml -d /home/hgcal/data/{modulename} -I >/dev/null 2>&1')

runs = glob.glob(f'/home/hgcal/data/{modulename}/pedestal_run/*')
#use the last run
make_hexmap_plots_from_file(f'{runs[-1]}/pedestal_run0.root', figdir=f'/home/hgcal/data/{modulename}')
print(f'>> Summary plots located in ~/data/{modulename}.')
