import time
import os
import subprocess
import sys
import glob
from time import sleep

import yaml
configuration = {}
with open('configuration.yaml', 'r') as file:
    configuration = yaml.safe_load(file)

sys.path.insert(1, configuration['HexmapPath'])
from plot_summary import make_hexmap_plots_from_file

modulename = '320-ML-F3CX-CM-0001'
ind = -1

runs = glob.glob(f'{configuration["DataLoc"]}/{modulename}/pedestal_run/*')
runs.sort()
labelind = ind if ind != -1 else len(runs)-1
label = f'{modulename}_run{labelind}'

make_hexmap_plots_from_file(f'{runs[ind]}/pedestal_run0.root', figdir=f'{configuration["DataLoc"]}/{modulename}', label=label)
