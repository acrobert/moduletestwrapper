from Keithley2400 import Keithley2400
import numpy as np
import time
import matplotlib.pyplot as plt
import pickle
from argparse import ArgumentParser
from datetime import datetime 
import os
#import psycopg2
from postgres_tools_testing import upload_PostgreSQL
import pandas as pd
import glob
import uproot3 as uproot
import asyncio
import asyncpg

import yaml
configuration = {}
with open('configuration.yaml', 'r') as file:
    configuration = yaml.safe_load(file)

def save_and_upload(datadict, modulename, inspector, upload=False):

    if upload:
        dt = datadict['datetime']
        data = datadict['data']
        temp_c = '21'
        db_upload_iv = [modulename, datadict['RH'], datadict['Temp'], 
                        data[:,0].tolist(), data[:,1].tolist(), data[:,2].tolist(), data[:,3].tolist(), 
                        dt.date(), dt.time(), inspector, '']
        upload_PostgreSQL(table_name = 'module_iv_test', db_upload = db_upload_iv)

    os.system(f'mkdir -p {configuration["DataLoc"]}/{modulename}')
    with open(f'{configuration["DataLoc"]}/{modulename}/{modulename}_IVset_{datadict["date"]}_{datadict["time"]}_{datadict["RH"]}.pkl', 'wb') as datafile:
        pickle.dump(datadict, datafile)


def add_mapping(df, hb_type = "LD"):
    # create dataFrame clone to avoid conflict                                                                                                                                                             
    df_data = df
    s = ""
    if not os.getcwd().endswith('hexmap'):
        s = 'hexmap/'
    # geometry mapping files' paths                                                                                                                                                                        
    ld_board_geo = s + "geometries/hex_positions_HPK_198ch_8inch_edge_ring_testcap.txt"
    hd_board_geo = s + "geometries/hex_positions_HPK_432ch_8inch_edge_ring_testcap.txt"

    # pad - channel mapping files' paths                                                                                                                                                                   
    ld_board_chan = s + "channel_maps/ld_pad_to_channel_mapping.csv"
    hd_board_chan = s + "channel_maps/hd_pad_to_channel_mapping_V2p1.csv"

    # import mapping files to pandas dataFrames and transform to python dicts                                                                                                                              
    if hb_type == "LD":
        chan_map_fname = ld_board_chan
        geo_fname = ld_board_geo
    elif hb_type == "HD":
        chan_map_fname = hd_board_chan
        geo_fname = hd_board_geo

    df_ch_map = pd.read_csv(chan_map_fname)
    d_ch_map = df_ch_map.set_index(["ASIC", "Channel", "Channeltype"]).to_dict()

    df_pad_map = pd.read_csv(geo_fname, skiprows= 7, delim_whitespace=True, names = ['padnumber', 'xposition', 'yposition', 'type', 'optional'])
    df_pad_map = df_pad_map[["padnumber","xposition","yposition"]].set_index("padnumber")
    d_pad_map = df_pad_map.to_dict()

    # add mapping to the data dataFrames                                                                                                                                                                   
    df_data["pad"] = df_data.apply(lambda x: get_pad_id(d_ch_map, x.chip, x.channel, x.channeltype), axis = 1)
    df_data["x"] = df_data["pad"].map(d_pad_map["xposition"])
    df_data["y"] = df_data["pad"].map(d_pad_map["yposition"])

    return df_data

def get_pad_id(map_dict, chip, chan, chantype):
    if (chip, chan, chantype) in map_dict["PAD"]:
        return map_dict["PAD"][(chip, chan, chantype)]
    else:
        return 0


def pedestal_upload(modulename, RH = '0', ind=-1):

    runs = glob.glob(f'{configuration["DataLoc"]}/{modulename}/pedestal_run/*')
    runs.sort()
    fname = runs[-1]+'/pedestal_run0.root'

    print(">> Uploading pedestal run of %s board from summary file %s into database" %(modulename, fname))

    # Open the hex data ".root" file and turn the contents into a pandas DataFrame.
    f = uproot.open(fname)
    try:
        tree = f["runsummary"]["summary"]
        df_data = tree.pandas.df()
    except:
        print("No tree found!")
        return 0

    df_data = add_mapping(df_data, hb_type = 'LD')

    Temp = '21'
    now = datetime.now()
    db_upload_ped = [modulename, RH, Temp, df_data['chip'].tolist(), df_data['channel'].tolist(), 
                     df_data['channeltype'].tolist(), df_data['adc_median'].tolist(), df_data['adc_iqr'].tolist(), 
                     df_data['tot_median'].tolist(), df_data['tot_iqr'].tolist(), df_data['toa_median'].tolist(), 
                     df_data['toa_iqr'].tolist(), df_data['adc_mean'].tolist(), df_data['adc_stdd'].tolist(), 
                     df_data['tot_mean'].tolist(), df_data['tot_stdd'].tolist(), df_data['toa_mean'].tolist(), 
                     df_data['toa_stdd'].tolist(), df_data['tot_efficiency'].tolist(), df_data['tot_efficiency_error'].tolist(), 
                     df_data['toa_efficiency'].tolist(), df_data['toa_efficiency_error'].tolist(), df_data['pad'].tolist(), 
                     df_data['x'].tolist(), df_data['y'].tolist(), now.date(), now.time(), 'acrobert', 'First upload']

    if 'BV' in runs[ind] and '320-M' in modulename:
        BV = runs.split('BV').rstrip('\n ')
        db_upload_ped.insert(3, BV)
    elif '320-M' in modulename:
        BV = -1
        db_upload_ped.insert(3, BV)
    else:
        pass

    table = 'module_pedestal_test' if ('320-M' in modulename) else 'hxb_pedestal_test'

    coro = upload_PostgreSQL(table_name = table, db_upload_data = db_upload_ped)
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(coro)

    #asyncio.run(upload_PostgreSQL(table_name = 'module_pedestal_test', db_upload_data = db_upload_ped))
    #upload_PostgreSQL(table_name = 'module_pedestal_test', db_upload = db_upload_ped)

def iv_upload(modulename, date, tag=''):

    # Glob for stored curves based on arguments
    print(f'{configuration["DataLoc"]}/{modulename}/{modulename}_IVset_{date}_{tag}_*.pkl')
    pickles_to_load = glob.glob(f'{configuration["DataLoc"]}/{modulename}/{modulename}_IVset_{date}_{tag}_*.pkl')
    print(f'>> Loading pickles: {pickles_to_load}')

    # Upload
    for pick in pickles_to_load:
        datadict = pickle.load(open(pick, 'rb'))
        data = datadict['data']

        RH = pick.split('_')[-1].split('.')[0]
        
        db_upload_iv = [modulename, RH, data[:,0].tolist(), data[:,1].tolist(), data[:,2].tolist(), data[:,3].tolist(), 'acrobert', 'First upload']
        upload_PostgreSQL(table_name = 'module_iv_test', db_upload = db_upload_iv)

# example of how to read
#coro = fetch_PostgreSQL('module_pedestal_test')
#loop = asyncio.get_event_loop()
#result = loop.run_until_complete(coro)
#
#for r in result: 
#    print(r)
