import psycopg2
from psycopg2 import sql

def get_query(table_name):
    if table_name == 'module_pedestal_test':
        query = f""" 
    INSERT INTO {table_name} 
    (module_name, rel_hum, bias_vol, chip, channel, channeltype, adc_median, adc_iqr, tot_median, tot_iqr, toa_median, toa_iqr, adc_mean, adc_stdd, tot_mean, tot_stdd, toa_mean, toa_stdd, tot_efficiency, tot_efficiency_error, toa_efficiency, toa_efficiency_error, pad, x, y, inspector, comment)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """
    elif table_name == 'module_iv_test':
        query = f""" 
    INSERT INTO {table_name} 
    (module_name, rel_hum, prog_v, meas_v, meas_i, meas_r, inspector, comment)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
    """
    else:
        query = None
    return query

def connect_PostgreSQL():

    conn = psycopg2.connect(
        host = 'cmsmac04.phys.cmu.edu',
        database = 'hgcdb',
        user = 'postgres',
        password = 'hgcal')

    cursor = conn.cursor()
    print('Connection successful.')



def upload_PostgreSQL(table_name,
                      db_upload):
    
    # conn = connect_db()

    conn = psycopg2.connect(
        host = 'cmsmac04.phys.cmu.edu',
        database = 'hgcdb',
        user = 'postgres',
        password = 'hgcal')
    
    cursor = conn.cursor()
    print('Connection successful. \n')

    schema_name = 'public'
    table_exists_query = sql.SQL("SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = %s AND table_name = %s);")
    cursor.execute(table_exists_query, [schema_name, table_name])
    table_exists = cursor.fetchone()[0]## this gives true/false
    
    query = get_query(table_name)
    
    data = tuple(db_upload)
    if table_exists:
        cursor.execute(query, data)
        conn.commit()
        print(f'Data is successfully uploaded to the {table_name}!')
    
    else:
        print(f'Table {table_name} does not exist in the database.')
        
    ## close connection
    cursor.close()
    conn.close()
    
    return None
