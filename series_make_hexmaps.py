from CentosPC import static_make_hexmaps
import glob
import sys

import yaml
configuration = {}
with open('configuration.yaml', 'r') as file:
    configuration = yaml.safe_load(file)


dirpattern = r'320-ML-F3CX-CM-0001'
datadirs = glob.glob(f'{configuration["DataLoc"]}/{dirpattern}')
modulenames = [dr.split('/')[-1].strip('\n') for dr in datadirs]
print(datadirs, modulenames)

#dir = 'TWL_006'
#datadirs = glob.glob(f'{configuration["DataLoc"]}/{dir}/pedestal_run/*')

for i in range(len(datadirs)):

    for j in range(12, 16):
        static_make_hexmaps(modulenames[i], j)
