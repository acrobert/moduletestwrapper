import PySimpleGUI as sg
from TrenzTestStand import TrenzTestStand
from CentosPC import CentosPC
from Keithley2400 import Keithley2400
from time import sleep

lgfont = ('Arial', 30)
sg.set_options(font=("Arial", 15))
DEBUG_MODE = True

def do_something_window(instruction, button, title=None):

    if title == None:
        title = instruction
    
    layout = [[sg.Text(instruction, font=lgfont)], [sg.Button(button)]]
    window = sg.Window(f"Module Test: {instruction}", layout, margins=(200,100))

    while True:
        event, values = window.read()
        if event == button or event == sg.WIN_CLOSED:
            break

    window.close()

def waiting_window(message, title=None):

    if title == None:
        title = message

    layout = [[sg.Text(message, font=lgfont)]]
    window = sg.Window(f"Module Test: {message}", layout, margins=(200,100))

    event, values = window.read(timeout=100)
    return window

    
#def end_session(dcdc_connected = False, dcdc_powered = False, trophy_connected = False, hexactrl_connected = False, hexactrl_powered = False, ts=None):
def end_session(state):

    ending = waiting_window("Ending session...")
    sleep(2)
    
    if state['-DCDC-Connected-']:
        if state['-DCDC-Powered-']:

            do_something_window("Disconnect DCDC power (green)", "Disconnected")

            if state['-Hexactrl-Connected-']:
                if state['-Hexactrl-Powered-']:
                    if state['-Hexactrl-Accessed-']:
                        shutdown = waiting_window("Shutting down hexacontroller...")
                        ts.shutdown()
                        sleep(5)
                        shutdown.close()
                    
                    do_something_window("Disconnect hexacontroller power (blue)", "Disconnected")

                do_something_window("Disconnect hexacontroller from trophy", "Disconnected")

        do_something_window("Disconnect DCDC", "Disconnected")

    if trophy_connected:

        do_something_window("Disconnect trophy and loopback", "Disconnected")
    
    sleep(2)
    ending.close()
    exit()

def LEDIndicator(key=None, radius=30):
    return sg.Graph(canvas_size=(radius, radius),
                    graph_bottom_left=(-radius, -radius),
                    graph_top_right=(radius, radius),
                    pad=(0, 0), key=key, visible=True)

def SetLED(window, key, color, empty=False):
    graph = window[key]
    graph.erase()
    if not empty:
        graph.draw_circle((0, 0), 12, fill_color=color, line_color=color)
    else:
        graph.draw_circle((0, 0), 12, fill_color=None, line_color=color)

livemoduleonly = [[sg.Text('Sensor Thickness'),
                   sg.Radio('120 micron', 6, key='-120-', enable_events=True),
                   sg.Radio('200 micron', 6, key='-200-', default=True, enable_events=True),
                   sg.Radio('300 micron', 6, key='-300-', enable_events=True)],
                  [sg.Text('Baseplate Type'),
                   sg.Radio('PCB', 5, key='-PCB-', enable_events=True),
                   sg.Radio("Carbon Fiber", 5, key='-CF-', default=True, enable_events=True),
                   sg.Radio("Copper-Tungsten", 5, key='-CuW-', enable_events=True)],
                  [sg.Checkbox('Preseries Module', default=True, key='-Preseries-', enable_events=True)]]

hexaboardonly = [[sg.Text('Hexaboard version: '), sg.Radio('V3', 4, key="-V3-", default=True, enable_events=True), sg.Radio('Production', 4, key="-Prod-", enable_events=True)],
                 [sg.Text("Hexaboard manufacturer: "), sg.Input(s=5, key='-HB-Manufacturer-', enable_events=True)]]

    
modulesetup = [[sg.Radio('Live Module', 1, key="-IsLive-", enable_events=True), sg.Radio('Hexaboard', 1, key='-IsHB-', enable_events=True)],
               [sg.Text('Hexaboard Density: '), sg.Radio('Low', 2, key="-LD-", default=True, enable_events=True), sg.Radio('High', 2, key="-HD-", enable_events=True)],
               [sg.Text('Hexaboard shape: '), sg.Radio('Full', 3, key='-Full-', default=True, enable_events=True), sg.Radio("Top", 3, enable_events=True, key='-Top-'),
                sg.Radio("Bottom", 3, enable_events=True, key='-Bottom-'), sg.Radio("Left", 3, enable_events=True, key='-Left-'),
                sg.Radio("Right", 3, enable_events=True, key='-Right-'), sg.Radio("Five", 3, enable_events=True, key='-Five-')],
               [sg.pin(sg.Column(livemoduleonly, key='-LM-Menu-', visible=False))],
               [sg.pin(sg.Column(hexaboardonly, key='-HB-Menu-', visible=False))],
               [sg.Text("Module Index: "), sg.Input(s=5, key='-Module-Index-', enable_events=True)],
               [sg.Text("Module Serial Number: "), sg.Text('', key='-Module-Serial-')],
               [sg.Text("Test Stand IP: "), sg.Combo(['cmshgcaltb4.lan.local.cmu.edu'], default_value='cmshgcaltb4.lan.local.cmu.edu', key="-TrenzHostname-")],
               [sg.Button("Configure Test Stand"), sg.Button('Only IV Test')]]

testsetup = [[sg.Text('Tests to run: ')],
             [sg.Checkbox('Pedestal Run', key='-Pedestal-Run-', default=True), sg.Text('Bias Voltage: '), sg.Input(s=5, key='-Bias-Voltage-Pedestal-')],
             [sg.Checkbox('Second Pedestal Run', key='-Pedestal-Run-2-'), sg.Text('Bias Voltage: '), sg.Input(s=5, key='-Bias-Voltage-Pedestal2-')],
             [sg.Checkbox('Full Test', key='-Full-Test-'), sg.Text('Bias Voltage: '), sg.Input(s=5, key='-Bias-Voltage-Full-')],
             [sg.Checkbox('Ambient IV Curve', key='-Ambient-IV-')],
             [sg.Checkbox('Dry IV Curve', key='-Dry-IV-')],
             [sg.Button("Run Tests", disabled=True, key='Run Tests')]]

statusbar = [[sg.Text("HV Cable Connected: "), sg.Push(), LEDIndicator(key='-HV-Connected-')],
             [sg.Text("HV Output On: "), sg.Push(), LEDIndicator(key='-HV-Output-On-')],
             
             [sg.Text("DCDC Connected: "), sg.Push(), LEDIndicator(key='-DCDC-Connected-')],
             [sg.Text("DCDC Powered: "), sg.Push(), LEDIndicator(key='-DCDC-Powered-')],
             [sg.Text("Trophy Connected: "), sg.Push(), LEDIndicator(key='-Trophy-Connected-')],
             [sg.Text("Hexacontroller Connected: "), sg.Push(), LEDIndicator(key='-Hexactrl-Connected-')],
             [sg.Text("Hexacontroller Powered: "), sg.Push(), LEDIndicator(key='-Hexactrl-Powered-')],
             [sg.Text("Hexacontroller Accessed: "), sg.Push(), LEDIndicator(key='-Hexactrl-Accessed-')],
             
             [sg.Text("Firmware Loaded: "), sg.Push(), LEDIndicator(key='-FW-Loaded-')],
             [sg.Text("DAQ Server: "), sg.Push(), LEDIndicator(key='-DAQ-Server-')],
             [sg.Text("I2C Server: "), sg.Push(), LEDIndicator(key='-I2C-Server-')],
             [sg.Text("DAQ Client: "), sg.Push(), LEDIndicator(key='-DAQ-Client-')]]

leftcol = sg.Frame('', [[sg.Frame('Module Setup', modulesetup, key='-Module-Setup-')], [sg.Frame('Select Tests', testsetup)]])
rightcol = sg.Frame('Status Bar', statusbar)

layout = [[sg.Text("CMU Module Testing GUI (WIP)", font=lgfont)], [sg.Text("DO NOT CONNECT ANYTHING")],
          [leftcol, rightcol]]

# Create the window
basewindow = sg.Window("Module Test: Start", layout, margins=(200,100), finalize=True, resizable=True)
event, values = basewindow.read(timeout=100)
basewindow.maximize()

ledlist = ['-HV-Connected-', '-HV-Output-On-', '-DCDC-Connected-', '-DCDC-Powered-', '-Trophy-Connected-', '-Hexactrl-Connected-', '-Hexactrl-Powered-',
           '-Hexactrl-Accessed-', '-FW-Loaded-', '-DAQ-Server-', '-I2C-Server-', '-DAQ-Client-' ]
for led in ledlist:
    SetLED(basewindow, led, 'black', empty=True)

def toggle_module_setup(enabled):
    keys = ['-IsLive-', '-IsHB-', '-LD-', '-HD-', '-Full-', '-Top-', '-Bottom-', '-Left-', '-Right-', '-Five-', '-120-', '-200-', '-300-', '-PCB-', '-CF-', '-CuW-', '-Preseries-',
            '-V3-', '-Prod-', '-HB-Manufacturer-', '-Module-Index-', '-TrenzHostname-']
    for key in keys:
        basewindow[key].update(disabled=(not enabled))

def enable_module_setup():
    toggle_module_setup(True)
def disable_module_setup():
    toggle_module_setup(False)

modulename = ''
trenzhostname = ''
livemodule = None

empty = ''
majortype = ['X', 'L']
minortype = ['F', '2', 'C', '']
macserial = 'CM'
moduleindex = ''
vendorserial = ''
moduleserial = ''

# Create an event loop
while True:
    event, values = basewindow.read()
    basewindow.maximize()

    if values['-IsLive-']: majortype[0] = 'M'
    if values['-IsHB-']: majortype[0] = 'X'

    basewindow['-LM-Menu-'].update(visible=values['-IsLive-'])
    basewindow['-HB-Menu-'].update(visible=values['-IsHB-'])
    
    if values['-LD-']: majortype[1] = 'L'
    if values['-HD-']: majortype[1] = 'H'

    if values['-Full-']: minortype[0] = 'F'
    if values['-Top-']: minortype[0] = 'T'
    if values['-Bottom-']: minortype[0] = 'B'
    if values['-Left-']: minortype[0] = 'L'
    if values['-Right-']: minortype[0] = 'R'
    if values['-Five-']: minortype[0] = '5'

    if values['-IsLive-']:
        if values['-120-']: minortype[1] = '1'
        if values['-200-']: minortype[1] = '2'
        if values['-300-']: minortype[1] = '3'
    
        if values['-PCB-']: minortype[2] = 'P'
        if values['-CF-']: minortype[2] = 'C'
        if values['-CuW-']: minortype[2] = 'W'
    
        if values['-Preseries-']: minortype[3] = 'X'
        if not values['-Preseries-']: minortype[3] = ''
        
    elif values['-IsHB-']:
        if values['-V3-']: minortype[1] = '0'
        if values['-V3-']: minortype[2] = '3'
        if values['-Prod-']: minortype[1] = '1'
        if values['-Prod-']: minortype[2] = '0'
        minortype[3] = ''
        
        vendorid = values['-HB-Manufacturer-'].rstrip()
        
    mind = values['-Module-Index-'].rstrip()
    if mind != '' and mind.isnumeric():
        if int(mind) >= 0 and int(mind) < 100000 and len(mind) == 5:
            moduleindex = mind
        elif int(mind) >= 0 and int(mind) < 100000 and len(mind) < 5:
            moduleindex = mind.zfill(5)
    else:
        moduleindex = ''    

        
    if values['-IsLive-']:
        moduleserial = f'320_{empty.join(majortype)}_{empty.join(minortype)}_{macserial}_{moduleindex}'
    elif values['-IsHB-']:
        moduleserial = f'320_{empty.join(majortype)}_{empty.join(minortype)}_{vendorid}_{moduleindex}'
        
    basewindow['-Module-Serial-'].update(value=moduleserial)
    
    if event == "Configure Test Stand":
        if moduleindex == '' or values['-TrenzHostname-'].rstrip() == '':
            continue
        if values['-IsHB-'] and vendorid == '':
            continue
        modulename = moduleserial
        livemodule = values['-IsLive-']
        trenzhostname = values['-TrenzHostname-'].rstrip()

        disable_module_setup()
        
        break

    if event == 'Only IV Test':
        if values['-IsHB-']:
            continue
        
    
    if event == sg.WIN_CLOSED:
        exit()

current_state = {}
    
for led in ledlist:
    current_state[led] = False
    SetLED(basewindow, led, 'black')

current_state['ts'] = None
current_state['pc'] = None
current_state['ts'] = None

def update_state(field, val, color=None):
    current_state[field] = val
    if field[0] == '-':
        assert color is not None
        SetLED(basewindow, field, color)

resistances = {'P1V2D': None, 'P1V2A': None, 'P1V5C': None, 'P1V5D': None}
layout = [[sg.Text("Check hexaboard resistances for shorts", font="Any 15")], [sg.Text("Input optional")],
          [sg.Text("P1V2D: "), sg.Input(s=5, key="-P1V2D-val-"), sg.Combo(['ohm', 'kohm', 'Mohm'], key="-P1V2D-unit-")],
          [sg.Text("P1V2A: "), sg.Input(s=5, key="-P1V2A-val-"), sg.Combo(['ohm', 'kohm', 'Mohm'], key="-P1V2A-unit-")],
          [sg.Text("P1V5C: "), sg.Input(s=5, key="-P1V5C-val-"), sg.Combo(['ohm', 'kohm', 'Mohm'], key="-P1V5C-unit-")],
          [sg.Text("P1V5D: "), sg.Input(s=5, key="-P1V5D-val-"), sg.Combo(['ohm', 'kohm', 'Mohm'], key="-P1V5D-unit-")],
          [sg.Button("Continue (no shorts)"), sg.Button("Found a short")]]

window = sg.Window("Module Test: Check Hexaboard", layout, margins=(200,100))

while True:
    event, values = window.read()
    if event == "Continue (no shorts)":
        for key in resistances.keys():
            if values[f'-{key}-val-'] != '':
                resval = float(values[f'-{key}-val-']) 
                resunit = values[f'-{key}-unit-']

                if resunit == 'ohm':
                    resistances[key] = resval
                elif resunit == 'kohm':
                    resistances[key] = resval*1000
                elif resunit == 'Mohm':
                    resistances[key] = resval*1000000
                    
        break
    if event == sg.WIN_CLOSED or event == "Found a short":
        exit()

window.close()

#ps = None
#leakage_current = {0: None, 1: None, 10: None, 100: None, 300: None, 600: None}
#if livemodule:
#
#    keith = waiting_window('Connecting to Keithley...')
#    ps = Keithley2400()
#    keith.close()
#
#    ivprobe = waiting_window('Verifying module IV behavior...')
#    ps.setVoltage(0.)
#    ps.outputOn()
#    for key in leakage_current.keys():
#        ps.setVoltage(key)
#        _, current, _ = ps.measureCurrent()
#        leakage_current[key] = current
#        if np.abs(current)*1000000. > 1. and key < 500:
#            end_session() ### FIX!
#        
#    ivprobe.close()
    

do_something_window("Connect DCDC to hexaboard", "Connected", title="Connect DCDC")
update_state('-DCDC-Connected-', True, 'blue')
do_something_window("Connect DCDC power cable (green)", "Connected", title="Power DCDC")
update_state('-DCDC-Powered-', True, 'red')

print(current_state)

layout = [[sg.Text("Measure voltage on pads", font=lgfont)], [sg.Text("Input optional")],
          [sg.Text("P1V2D: "), sg.Input(s=5, key="-P1V2D-vltg-"), sg.Text("V; expect 1.2-1.25V")],
          [sg.Text("P1V2A: "), sg.Input(s=5, key="-P1V2A-vltg-"), sg.Text("V; expect 1.2-1.25V")],
          [sg.Text("P1V5C: "), sg.Input(s=5, key="-P1V5C-vltg-"), sg.Text("V; expect 1.47-1.5V")],
          [sg.Text("P1V5D: "), sg.Input(s=5, key="-P1V5D-vltg-"), sg.Text("V; expect 1.47-1.5V")],
          [sg.Button("As expected"), sg.Button('Power incorrect')]]

window = sg.Window("Module Test: Probe power pads", layout, margins=(200,100))

while True:
    event, values = window.read()
    if event == "As expected":
        break
    if event == sg.WIN_CLOSED or event == "Power incorrect":
        end_session(dcdc_connected=True, dcdc_powered=True)	

window.close()

do_something_window("Disconnect DCDC power cable (green)", "Disconnected", title="Depower DCDC")
update_state('-DCDC-Powered-', False, 'black')
do_something_window("Connect trophy board and loopback board to hexaboard", "Connected", title="Connect Trophy and Loopback")
update_state('-Trophy-Connected-', True, 'blue')
do_something_window("Ensure NOTHING is powered", "No power")
do_something_window("Connect trophy board and hexacontroller", "Connected", title='Connect Hexacontroller')
update_state('-Hexactrl-Connected-', True, 'blue')
do_something_window("Connect hexacontroller power cable (blue)", "Connected", title='Power Hexacontroller')
update_state('-Hexactrl-Powered-', True, 'red')

connecting = waiting_window("Connecting to hexacontroller...")
if DEBUG_MODE:
    sleep(5)
    ts = None
else:
    ts = TrenzTestStand(trenzhostname)
connecting.close()
update_state('-Hexactrl-Accessed-', True, 'blue')
update_state('ts', ts)

do_something_window("Connect DCDC power cable (green)", "Connected", title="Power DCDC")
update_state('-DCDC-Powered-', True, 'red')
loading = waiting_window("Loading firmware on hexacontroller...", title="Loading Firmware...")
if DEBUG_MODE:
    sleep(5)
    fwloaded = True
else:
    fwloaded = ts.loadfw()
loading.close()
if not fwloaded:
    end_session(dcdc_connected=True, dcdc_powered=True, trophy_connected=True, hexactrl_connected=True, hexactrl_powered=True)

starting = waiting_window("Starting services on test stand...", title="Starting Services...")
if DEBUG_MODE:
    sleep(5)
    services = True
else:
    services = ts.startservers()
starting.close()
if not services:
    end_session(dcdc_connected=True, dcdc_powered=True, trophy_connected=True, hexactrl_connected=True, hexactrl_powered=True)

daq = waiting_window("Starting services on PC...", title="Starting Services...")
if DEBUG_MODE:
    pc = None
    sleep(5)
else:
    pc = CentosPC(trenzhostname, modulename, livemodule)
daq.close()
update_state('pc', pc)

ready = waiting_window("Ready to run tests.", title="Ready")
sleep(2)
ready.close()

print(current_state)

basewindow['Run Tests'].update(disabled=False)
event, values = basewindow.read(timeout=100)
takePedestals = False
takePedestals2 = False
pedestalBV = None
pedestalBV2 = None
while True:
    event, values = basewindow.read()
    basewindow.maximize()
    
    if event == sg.WIN_CLOSED or event== 'Run Tests':
        basewindow['Run Tests'].update(disabled=True)
        if values['-Bias-Voltage-Pedestal-'].rstrip() != '': pedestalBV = int(values['-Bias-Voltage-Pedestal-'].rstrip())
        if values['-Bias-Voltage-Pedestal-'].rstrip() != '': pedestalBV2 = int(values['-Bias-Voltage-Pedestal2-'].rstrip())
        if values['-Pedestal-Run-']: takePedestals = True
        if values['-Pedestal-Run-2-']: takePedestals2 = True
        break

if takepedestals:

    pedestals = waiting_window("Running Pedestals...")

    if livemodule and pedestalBV is not None:
        ps.setVoltage(pedestalBV)
        ps.outputOn()
    pc.pedestal_run()
    if livemodule:
        ps.outputOff()
    pc.make_hexmaps()
